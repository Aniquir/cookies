package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
@WebServlet(value = "/readcookies")
public class ReadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        HtmlGenerator.generateHeader(out, "ReadServlet");
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            printCookies(out, cookies);
        } else {
            out.println("No cookie found");
        }
        HtmlGenerator.generateFooter(out);
    }

    private void printCookies(PrintWriter out, Cookie[] cookies) {
        out.println("<ul>");
        for (Cookie cookie : cookies) {
            out.println("<li>");
            printCookie(out, cookie);
            out.println("</li>");
        }
        out.println("</ul>");
    }

    private void printCookie(PrintWriter out, Cookie cookie) {
        out.println("Name: " + cookie.getName()
                + ", value: " + cookie.getValue()
                + ", max age: " + cookie.getMaxAge()
                + ", version: " + cookie.getVersion());
    }

}
