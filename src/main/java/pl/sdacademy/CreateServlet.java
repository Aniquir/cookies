package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
@WebServlet(value = "/createcookies")
public class CreateServlet extends HttpServlet {

    private final static String[] COOKIE_NAMES = {"language",
            "backgroundcolor",
            "textcolor",
            "textsize"};

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (String name : COOKIE_NAMES) {
            addCookie(name, req, resp);
        }
        PrintWriter out = resp.getWriter();
        HtmlGenerator.generateHeader(out, "CreateServlet");
        out.println("Settings saved");
        HtmlGenerator.generateFooter(out);
    }

    private void addCookie(String name, HttpServletRequest req, HttpServletResponse resp) {
        String value = req.getParameter(name);
        Cookie cookie = new Cookie(name, value);
        resp.addCookie(cookie);
    }

}
